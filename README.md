### Learning Next.js ###

[Learning Next.js](https://learnnextjs.com/)

- Server-rendered by default
- Automatic code splitting for faster page loads
- Simple client-side routing (page based)
- Webpack-based dev environment which supports [Hot Module Replacement (HMR)](https://webpack.js.org/concepts/hot-module-replacement/)
- Able to implement with Express or any other Node.js HTTP server
- Customizable with Babel and Webpack configurations